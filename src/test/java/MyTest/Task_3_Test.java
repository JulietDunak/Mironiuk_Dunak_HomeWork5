package MyTest;
import Task_3.Circle;
import Task_3.GeometricFigure;
import Task_3.Square;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
public class Task_3_Test {
    GeometricFigure square;
    GeometricFigure circle;
    @BeforeMethod
    public void beforeTest() {
        square = new Square();
        circle = new Circle();
    }
 @Test (expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Side < 0")
 public void testNegativeSquareGetArea() throws IllegalArgumentException {
    square.getArea(-1);
   }
    @Test
    public void testPositiveSquareGetArea(){
        Assert.assertEquals(6.25, square.getArea(2.5));
    }
    @Test (expectedExceptions = IllegalArgumentException.class,
            expectedExceptionsMessageRegExp = "Side < 0")
    public void testNegativeSquareGetPerimeter () throws IllegalArgumentException {
       square.getPerimeter(-1);
    }
    @Test
    public void testPositiveSquareGetPerimeter() throws Exception {
        Assert.assertEquals(10.00, square.getPerimeter(2.5));
    }
    @Test (expectedExceptions = IllegalArgumentException.class,
            expectedExceptionsMessageRegExp = "Radius < 0")
    public void testNegativeCircleGetArea() throws IllegalArgumentException {
       circle.getArea(-1);
    }
    @Test
    public void testPositiveCircleGetArea() throws Exception {
        Assert.assertEquals(14.387237716379818, circle.getArea(2.14));
    }
    @Test (expectedExceptions = IllegalArgumentException.class,
            expectedExceptionsMessageRegExp = "Radius < 0")
    public void testNegativeCircleGetPerimeter() throws IllegalArgumentException {
        circle.getPerimeter(-1);
    }
    @Test
    public void testPositiveCircleGetPerimeter() {
        Assert.assertEquals(13.446016557364315, circle.getPerimeter(2.14));
    }
}