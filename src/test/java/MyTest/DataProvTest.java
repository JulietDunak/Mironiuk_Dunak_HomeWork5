package MyTest;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProvTest {
//    public void getLenN(int lenth){
//        if(lenth<10){
//            throw new IllegalArgumentException("Bad");
//        }
//    }
    @DataProvider(name = "data-provider")
        public Object[][] dataprovider() {
            return new Object[][]{
                    {"Halo Headlights"},
                    {"Projector Headlights"},
                    {"Head1111111111111"},
                    {"U-Bar Headlights"},
                    {"DRL-Bar Headlights"}
            };
        }
        @Test (dataProvider = "data-provider")
        public void getLength(String line){
        Assert.assertTrue(line.length()>=10,"Good");
        }
//        @Test (dataProvider = "data-provider",expectedExceptions = IllegalArgumentException.class,expectedExceptionsMessageRegExp = "Bad")
//        public void getLength2(String line) throws IllegalArgumentException {
//
//         line.length();
//        }
    }