package MyTest;
import Task_1.CatImpl;
import Task_1.DogImpl;
import Task_1.IAnimal;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
public class Task_1_Test {
    IAnimal cat;
    IAnimal dog;
    @BeforeMethod
    public void beforeTest(){
        cat = new CatImpl();
        dog = new DogImpl();
    }
    @Test
    public  void testPositiveCatGetSound(){
        Assert.assertEquals("Cats Meow",cat.getSound("Cats Meow"));
    }
    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Cat should say Meow!")
    public  void testNegativeCatGetSound() throws IllegalArgumentException{
        cat.getSound("Gav");
    }
    @Test
    public  void testPositiveDogGetSound(){

        Assert.assertEquals("Dogs Gav",dog.getSound("Dogs Gav"));
    }
    @Test(expectedExceptions = IllegalArgumentException.class,expectedExceptionsMessageRegExp = "Dogs should say Gav!")
    public  void testNegativeDogGetSound() throws IllegalArgumentException{
                dog.getSound("Dogs Meow");
    }
}
