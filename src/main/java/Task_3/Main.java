package Task_3;

public class Main {
    public static void main(String[] args) {
    GeometricFigure circle = new Circle();
        System.out.println("The area of the circle is " + circle.getArea(2.14));
        System.out.println("The perimeter of the circle is " + circle.getPerimeter(2.14));
        GeometricFigure square = new Square();
        System.out.println("The area of the square is " + square.getArea(2.5));
        System.out.println("The perimeter of the square is " + square.getPerimeter(2.5));
}
    }
